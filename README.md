# Bootstrapping the lexicon: Toward bottom-up approaches to the study of individual vocabularies

We will develop a novel approach to the study of an individual's mental lexicon that is participant-driven, and less dependent on external norms and language standards. Present methods are "top-down" in that they use samples from the standardized vocabulary in formal dictionaries to probe that person's linguistic knowledge. This approach assumes (a) the existence of a standard, and (b) that items outside of that standard play a minor role in that person's vocabulary. We propose a bottom-up approach to studying individual vocabularies. British and American English speakers will complete a series of short word-association tasks where the stimuli in later sessions will be drawn from their responses in earlier sessions, thus allowing them to explore their own vocabulary space and provide us information about the structure of their individual lexicons that is not biased by external standards. This information will be of use to both language/education researchers and speech clinicians.

## Contributors
UK PI: Charles Redmon, University of Oxford  
US PI: Nichol Castro, University at Buffalo  


